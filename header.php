<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="<?php bloginfo('description'); ?>">

	<!-- Necessário para layout responsivo -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <style>
@import url('https://fonts.googleapis.com/css2?family=Lora:wght@500;700&family=Roboto+Condensed:wght@300;400;700&display=swap');
</style>

<!-- Font Awesome -->
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
  rel="stylesheet"
/>
<!-- Google Fonts -->
<link
  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
  rel="stylesheet"
/>
<!-- MDB -->
<link
  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.6.0/mdb.min.css"
  rel="stylesheet"
/>

  <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css"></script>
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" >
  <?php wp_head(); ?>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
  <?php wp_footer(); ?>
</head>
<!--
  <body <?php body_class(); ?>>

    <div class="wrapper">
      <header class="header" role="banner">

        <div class="logo">
          <?php the_custom_logo(); ?>
        </div>

        <nav class="navigation" role="navigation">
          <?php wp_nav_menu() ?>
        </nav>

      </header>
-->


<!-- Início do body -->
<body <?php body_class(); ?> >

<div style="width: 100%; height: 110px;  background-color: #1a2946; position: absolute;">

</div>

<div class="container"  style="margin:0 auto; position: relative;" >
<nav class="navbar navbar-expand-lg navstyle" >
   <a class="navbar-brand" href="<?php echo home_url();?>">
   <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png"  alt="logo" class="img-logo" >
   </a>
   <div class="ml-auto">
   <button class="navbar-toggler ml-auto custom-toggler"type="button" data-toggle="collapse" data-target="#navbarSupportedContent">
   <span class="navbar-toggler-icon"></span>
   </button>

   <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto ">
      <?php

        wp_nav_menu( array(
          'container'      => false,
          'menu_id'        => 'main-menu',
          'menu_class'     => 'navbar-nav ml-auto',
          'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
          'theme_location' => 'main-menu',
          'depth'          => 5,
          'fallback_cb'    => false,
          'walker'         => new Bootstrap_Menu_Walker()
        ) );
        ?>

      </ul>
   </div>
</nav>

<?php if ( !is_home() && !is_front_page() ) { ?>
   <div class="container">
   <div class="row justify-content-md-center  menu-horizontal" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/retangulo.png);" >
         <?php
            foreach ( wp_get_menu_itens_bar() as $navItem ) {
            echo '<div class="col-md-12 col-lg-3">';
               echo '<p >';
               echo '<a href="'.$navItem->url.'" title="'.$navItem->title.'" class="inline-menu">'.$navItem->title.'</a>';
               echo '</p>';
            echo '</div>';
            }
         ?>
      </div>

   </div>

<?php } ?>