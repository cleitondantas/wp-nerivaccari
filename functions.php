<?php

/**
 * Add theme support for various WordPress features.
 *
 * @return void
 */




function wp_nerivaccari_setup() {
    require_once get_template_directory().'/bootstrap_menu.php';
	// Support programmable title tag.
	add_theme_support( 'title-tag' );

	// Support custom logo.
	add_theme_support( 'custom-logo' );
	add_theme_support( 'post-thumbnails', array( 'post', 'page' ) );
	add_image_size( 'custom-size', 300, 400, true );
	/**
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on wp-nerivaccari, use a find and replace
	 * to change 'wp-nerivaccari' to the name of your theme in all the template files.
	 */
	register_nav_menu('main-menu', 'Main menu');
    register_nav_menu('itens-menu', 'Itens menu');
    load_theme_textdomain( 'wp-nerivaccari', get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'wp_nerivaccari_setup' );


/**
 * Register widget area.
 *
 * @return void
 */
function wp_nerivaccari_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'wp-nerivaccari' ),
			'id'            => 'sidebar-1',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<p class="widget-title xstrong">',
			'after_title'   => '</p>',
		) 
	);
	
	register_sidebar( array(
		'name'          => esc_html__( 'Ultimas Postagens', 'wp-nerivaccari' ),
		'id'            => 'latest_posts',
		'description'   => esc_html__( 'Add widgets Latest posts', 'latest_posts' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));

	register_sidebar( array(
		'name'          => esc_html__( 'Contato', 'wp-nerivaccari' ),
		'id'            => 'contact_posts',
		'description'   => esc_html__( 'Add widgets Contato', 'contact_posts' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	));



}
add_action( 'widgets_init', 'wp_nerivaccari_widgets_init' );


function wp_nerivaccari_customizer($wp_customize) {
    //adding section in wordpress customizer   
    $wp_customize->add_section('copyright_extras_section', array(
        'title' => 'Copyright Text Section'
    ));

    //adding setting for copyright text
    $wp_customize->add_setting('text_setting', array(
        'default' => 'Default Text For copyright Section',
    ));

    $wp_customize->add_control('text_setting', array(
        'label'   => 'Copyright text',
        'section' => 'copyright_extras_section',
        'type'    => 'text',
    ));
}

//adding setting for copyright text
add_action('customize_register', 'wp_nerivaccari_customizer');

function admin_bar(){

	if(is_user_logged_in()){
	  add_filter( 'show_admin_bar', '__return_true' , 1000 );
	}
  }
  add_action('init', 'admin_bar' );


  function wp_get_menu_itens_bar(){
    $menuLocations = get_nav_menu_locations(); // Get our nav locations (set in our theme, usually functions.php)   // This returns an array of menu locations ([LOCATION_NAME] = MENU_ID);
    $menuID = $menuLocations['itens-menu']; // Get the *primary* menu ID
    $primaryNav = wp_get_nav_menu_items($menuID); 
    return  $primaryNav;
  }





?>