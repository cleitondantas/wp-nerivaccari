<?php get_header(); ?>



    <main role="main" aria-label="Content">
    <div class="row justify-content-md-center justify-content-lg-center"> 
    <p> 
      <a href="#" style="color: #dbac55 !important;">
        <h2><?php single_cat_title() ?></h2><!-- Nome da categoria -->
      </a>
    </p>
      <div>
        <?php the_archive_description() ?><!-- Descrição da categoria -->
      </div>

      <?php if (have_posts()) {
      while (have_posts()) { the_post(); ?><!-- Loop de posts -->

        <div class="col-sm-12 col-lg-4" style="padding: 5px;" >
          <div class="card card-category-height"  >
          
          <a href="<?php the_permalink() ?>" style=" padding: 5px;text-align: center;"><!-- Link permanete -->
              <?php the_post_thumbnail('custom-size') ?><!-- Imagem em destaque -->
            </a>

          <div class="card-body">
          <p style="font-family: 'Lora', serif;font-size: 16pt; color:#dbac55 ;"  class="card-title">
                <a href="<?php the_permalink() ?>" style="color: #dbac55 !important;">
                <?php the_title() ?>
                </a>         
           </p>
          <p class="card-text-medium">
          <?= get_the_excerpt() ?>
          </p>

          <a href="<?php the_permalink() ?>" style="color: #dbac55 !important;">
              Leia mais...
              </a>
              </p>
          </div>
        </div>  
        </div>
        <?php } } ?>




      </div>
  </main>




<?php get_footer(); ?>
