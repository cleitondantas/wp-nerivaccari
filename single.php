<?php get_header(); ?>

<main role="main" aria-label="Content">
  <section>

    <?php if (have_posts()): while(have_posts()): the_post(); ?>
      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<!--
/*        <?php if ( has_post_thumbnail() ): ?>         */
          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
/*            <?php the_post_thumbnail(); ?>  */
          </a>
/*        <?php endif; ?> */
-->
        <h2>
        </br>
          <?php the_title_attribute(); ?>
          </br>
          </br>
        </h2>
        
        <p>
          <?php if ( has_post_thumbnail() ): ?>
            <img style="float:right; margin: 5px;" src="<?php echo get_the_post_thumbnail_url(); ?>" height="200">
          <?php endif; ?>
          <?php the_content(); ?>
        </p>

<!--
        <span class="comments"><?php if (comments_open(get_the_ID())) comments_popup_link(__('Leave your thoughts', 'wp-nerivaccari'), __('1 Comment', 'wp-nerivaccari'), __('% Comments', 'wp-nerivaccari')); ?></span>
-->        

        <?php the_tags(__('Tags: ', 'wp-nerivaccari' ), ', ', '<br>');?>
        <p><?php esc_html_e('Category: ', 'wp-nerivaccari'); the_category(', '); ?></p>

      </article>
    <?php endwhile; ?>
    <?php else : ?>
      <article>
        <h1><?php esc_html_e( 'Sorry, nothing to display.', 'wp-nerivaccari' ); ?></h1>
      </article>
    <?php endif; ?>
  </section>
</main>


<?php get_footer(); ?>
