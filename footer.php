<!-- Fecha o body -->
</body>
</br>
</br>
</br>
</br>

<!-- Footer -->
<div style="margin:0 auto;" >
<footer class="page-footer font-small blue pt-4 " style="background-color: #1a2946;  color: #ffffff;">

  <!-- Footer Links -->
  <div class="container-fluid text-md-left text-md-left" >

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-3 mt-md-0 mt-3">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" class="d-inline-block align-top" alt="logo" width="200" height="60" >
        <!-- Content -->
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-2 mb-md-0 mb-2">

        <!-- Links -->
        <ul class="list-unstyled">
          <li>
            <a href="#idescritorio" style="color: #ffffff !important;">Escritório</a>
          </li>
          <li>
            <a href="#idadvogados" style="color: #ffffff !important;">Advogados</a>
          </li>
          <li>
            <a href="#idconhecaseusdireitos" style="color: #ffffff !important;">Conheça seus direitos</a>
          </li>
          <li>
            <a href="#idcontato" style="color: #ffffff !important;" >Contato</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 mb-md-0 mb-4">

        <ul class="list-unstyled">
          <li>
            <a href="#!" style="color: #ffffff !important;" >Direitos do Passageiro Áereo</a>
          </li>
          <li>
            <a href="#!" style="color: #ffffff !important;" >Direitos do Consumidor</a>
          </li>
          <li>
            <a href="#!" style="color: #ffffff !important;">Direitos do Trabalhador</a>
          </li>
          <li>
            <a href="#!" style="color: #ffffff !important;">Outras Áreas de Atuação</a>
          </li>
        </ul>

      </div>
      <!-- Grid column -->
      <div class="col-md-3 mb-md-3 mb-3">

        <ul class="list-unstyled">
          <li>
           <!-- Facebook -->
          <a class="fb-ic">
            <i class="fab fa-facebook-f fa-lg white-text mr-md-2 mr-2 fa-1x"></i>
          </a>
          <!--Instagram-->
          <a class="ins-ic">
            <i class="fab fa-instagram fa-lg white-text mr-md-2 mr-2 fa-1x"> </i>
          </a>
        <!--Linkedin -->
            <a class="li-ic">
                <i class="fab fa-linkedin-in fa-lg white-text mr-md-2 mr-2 fa-1x"> </i>
            </a>
          </li>
        </ul>

      </div>
    </div>
    <!-- Grid row -->

  </div>
  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3 link" style="text-decoration: none !important;">
  <p class="footer__copyright copyright">
          &copy; <?php echo date('Y'); ?> <?php _e('', 'wp-nerivaccari') ?> <?php bloginfo('name'); ?>  Todos os Direitos Reservados. 
   </p>  

  <p class="footer__copyright copyright" style="font-size: 8pt;" >  Desenvolvido por: &nbsp;  
  <a href="https://www.rocketdevelopment.com.br/" style="color: #ffffff !important;"> Rocket Development</a>
    &nbsp;&nbsp;&nbsp; &nbsp;
    Agencia WM7:
    <a href="#" style="color: #ffffff !important;">
        &nbsp;
        @agenciawm7</a>
  </div>
  <!-- Copyright -->
</footer>
</div>

</html>