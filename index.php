<?php get_header(); ?>

<?php if ( is_home() && is_front_page() ) { ?>
<div style='position:relative; top:0px; left:0px;' >
   <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/banner-principal-02.png" class="d-inline-block align-top" alt="logo"  style="width:100%;"> 
   <div  class="banner-principal" >
      <p >
         Chegou a hora <br/>
         de você conhecer<br/> 
         e entender os <br/>
         seus direitos.
      </p>
   </div>
</div>
<div>

</div>
<div>
   <div class="container" >
      <div class="row justify-content-md-center  menu-horizontal" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/retangulo.png);" >
         <?php 
            foreach ( wp_get_menu_itens_bar() as $navItem ) {
            echo '<div class="col-md-12 col-lg-3">';
               echo '<p >';
               echo '<a href="'.$navItem->url.'" title="'.$navItem->title.'" class="inline-menu" >'.$navItem->title.'</a>';
               echo '</p>';
            echo '</div>';
            }  
         ?>
      </div>
   </div>
</div>
</br>





<div class="container" >

   <div class="row justify-content-md-center" >
      </br>
      <div class="col-lg-12" id="idescritorio">
         <p class="text-title" >
            Escritório
         </p>
         <div style="color:#dbac55">
            <p class="text-subtitle" style="font-size: 24pt !important;" >
               Bem Vindo ao </br>
               Neri & Vaccari
            </p>
         </div>
         </br>
      </div>
   </div>
   <div class="row justify-content-md-center" >
      <div class="col-lg-8" >
         <p class="text-blocs"  >
            Pensado e organizado para trazer clareza e a solução necessária quanto aos seus direitos
         </p>
         <p class="text-blocs" > 
            Escritório especializado em Direito do Consumidor, Direito do Passageiro Aéreo, Direito do Trabalho, Cível e Família, possui a missão de oferecer uma advocacia humanizada, objetiva e assertiva, de modo a proporcionar o devido conhecimento quanto aos direitos de seus clientes e auxiliá-los a obtê-los.  
         </p>

      </div>
      <div id="#martelinho" class="col-lg-4"   style="text-align: center;" >
         <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/martelinho.jpg" class="d-inline-block align-top"  alt="logo" class="img-referencia"  > 
      </div>
   </div>
   </br>



   <div class="row justify-content-md-center" >
      
      </br>
      <div class="col-lg-12" id="idadvogados" >
         <p class="text-title" >
            Advogados
         </p>
         <div >
            <p class="text-subtitle" >
               Conheça quem forma 
               nossa equipe
            </p>
         </div>
      </div>

      <div class="row justify-content-md-center" >
         <div class="col-sm-12 col-lg-12" >
         <p class="text-blocs" style="font-size: 20pt;" >
            Não basta ter formação, é preciso ter experiência e usá-la a favor de todos.
         </p>
         <p   class="text-blocs"> 
            Nossa equipe é formada por um corpo jurídico altamente qualificado, com especialistas nos mais diversos ramos do direito. Sempre agindo com empatia e respeito, nossos profissionais buscam sempre prestar o atendimento adequado a encontrar a solução necessária aos nossos clientes, entregando uma advocacia com qualidade e transparência. 
         </p>
      </div>
   </div>
</div>


 <div class="row justify-content-md-center justify-content-lg-center">

   <div class="col-sm-12 col-lg-6" >
      <div class="card card-height"  >
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/fotos/bruna_front.jpg" class="card-img-top"  alt="Kate Vaccari"  style="padding: 60px 60px;"> 
      <div class="card-body">
      <p style="font-family: 'Lora', serif;font-size: 24pt; color:#dbac55"  class="card-title">
            <a href="/category/advogados/nruna" style="color: #dbac55 !important;">
            Bruna Neri
            </a>         
         </p>
      <p class="card-text" style="font-size: large;"> 
         Sócia do escritório Neri & Vaccari.
      </p>
      <p class="card-text-medium">
         Formada em Direito pela Universidade Paulista em 2014, pós graduada em Direito Processual Civil pelo Instituto Damásio de Direito – Faculdade Ibmec São Paulo em 2022, com curso de atualização e prática em Direito do Consumidor, certificado pela Faculdade Ibmec São Paulo – Damásio Educacional em 2021, devidamente inscrita na Ordem dos Advogados do Brasil de São Paulo sob nº 356.310.
      </p>
      <p class="card-text-medium">
         Possui mais de 07 anos de experiência no direito, com forte atuação no contencioso cível e com direito do consumidor, principalmente em ações indenizatórias. 
      </p>
      <a href="/category/advogados/bruna" style="color: #dbac55 !important;">
         Leia mais...
         </a>
         </p>
      </div>
   </div>  
   </div>

      <div class="col-sm-12 col-lg-6">
         <div class="card card-height" >
         <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/fotos/kate_front.jpg" class="card-img-top"  alt="Kate Vaccari" style="padding: 60px 60px;"> 
         <div class="card-body">
         <p style="font-family: 'Lora', serif;font-size: 24pt; color:#dbac55"  class="card-title">
               <a href="/category/advogados/kate" style="color: #dbac55 !important;">
               Kate Vaccari
               </a>         
            </p>
            <p class="card-text" style="font-size: large;"> 
               Sócia do escritório Neri & Vaccari
            </p>
         <p class="card-text-medium">
            Formada em Direito pela Universidade Cruzeiro do Sul em 2006, pós graduada em Processo Civil e do Trabalho pela Escola Paulista de Direito em 2017. Inscrita na Ordem dos Advogados de São Paulo sob o nº 338.432.
         </p>
         <p class="card-text-medium" >
            Contando com mais de 10 anos de experiência e atuação no direito, atuando em escritórios de advocacia voltados para a defesa do consumidor, direito de família, societário, patrimonial e tributário. 
         </p>
            <a href="/category/advogados/kate" style="color: #dbac55 !important;">
               Leia mais...
               </a>  
            </p>
         </p>
         </div>
      </div>  
      </div>
</div>
</p>
   <div class="row justify-content-md-center" >
      <div class="col-lg-12" id="idconhecaseusdireitos" >
         <p class="text-title" >
            Conheça seus direitos
         </p>
         <div >
            <p class="text-subtitle" >
               Você realmente conhece os seus direitos?
            </p>
         </div>

      </div>


      <div class="row justify-content-md-center" >
         </br>
         <div class="col-lg-6">
         <p class="text-blocs"  >
            Para que uma sociedade viva em equilíbrio é 
            necessário todos conhecerem seus limites.
         </p>
<!-- O texto a baixo deve sair no mobile para dar espaço para o feed de ultimas postagens -->
         <p  class="text-blocs mobileoff" > 
            Muito se fala sobre direitos e deveres, mas, nem sempre estes são claros o suficiente para que todos possam coloca-los em prática no seu dia a dia.
         </p>
         <p class="text-blocs mobileoff" > 
            Pensando nisso, disponibilizamos neste espaço alguns textos com temas importantes para te auxiliar a melhor entender sobre seus direitos e deveres, buscando esclarecer as principais dúvidas. 
         </p>
      </div>
      <div class="col-lg-1">

      </div>

      <div class="col-lg-5"  >
         <div class="card" style="padding: 10% 7% 10% 7%;">
            <p style="  font-family: 'Lora', serif;">
               Ultimas postagens
            </p>
       
      <?php if( is_active_sidebar( 'latest_posts' )) : ?>
            <aside class="latest_posts">
               <?php dynamic_sidebar( 'latest_posts' ); ?>
            </aside>
      <?php endif; ?>
      </div>
</div>
   </div>
</br>
<p>
</br>
</p>
   <div class="row justify-content-md-center" >
      <div class="col-sm-12 col-lg-6 " style="padding: 10px 30px;" id="idcontato">
         <p class="text-title" >
            Contato
         </p>
         <div >
            <p class="text-subtitle" >
               Entre em contato para
               tirar suas dúvidas
            </p>
         </div>

         <div>
         <p class="text-blocs"  >
            Você tem algum caso que precisa de acompanhamento e solução? </br>
            Então, mande uma mensagem
         </p>
         </br>
         <div >
            <p style="font-family: 'Lora', serif;font-size: 15pt; color:#dbac55" >
               Você também pode ligar ou nos enviar um e-mail
            </p>
            <p class="text-blocs"  >
                (11) 9 6465-5050
            </p>
            <p class="text-blocs"  >
                  contato.nvadvogados@gmail.com
            </p>
         </div>
      </div>
   </br>
   </br>
   </br>
</div> 


<div class="col-sm-12 col-lg-6 form-group" style="padding: 10px 15%;" >
   </br>
      </br>
      <?php if( is_active_sidebar( 'contact_posts' )) : ?>
            <aside class="contact_posts">
               <?php dynamic_sidebar( 'contact_posts' ); ?>
            </aside>
      <?php endif; ?>
      </div>
   </div>
</div>
<?php } ?>
<!--
   /
   <main role="main" aria-label="Content">
     <section>
     //  <h1><?php _e('Latest posts', 'wp-nerivaccari'); ?></h1>
      // <?php get_template_part('loop'); ?>
      // <?php get_template_part('pagination'); ?>
     </section>
   </main>
   
   //Campo de Busca
   
   -->
<?php get_footer(); ?>